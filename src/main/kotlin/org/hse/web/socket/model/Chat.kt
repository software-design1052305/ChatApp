package org.hse.web.socket.model

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.util.*

@Table
data class Chat(
    @Id
    val id: UUID? = null,
    val name: String?
)