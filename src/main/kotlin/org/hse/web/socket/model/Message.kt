package org.hse.web.socket.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime
import java.util.*

@Table
data class Message(
    @Id
    var id: UUID? = null,
    var chatId: UUID? = null,
    var messageTime: LocalDateTime? = null,
    val content: String,
    val authorName: String
)