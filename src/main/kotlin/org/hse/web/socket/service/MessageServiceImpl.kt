package org.hse.web.socket.service

import org.hse.web.socket.model.Chat
import org.hse.web.socket.model.Message
import org.hse.web.socket.repository.ChatRepository
import org.hse.web.socket.repository.MessageRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class MessageServiceImpl(
    private val chatRepository: ChatRepository,
    private val messageRepository: MessageRepository
):MessageService {
    override fun createChat(chat: Chat): Chat {
        return chatRepository.save(chat)
    }

    override fun getChat(chatName: String): Chat {
        return chatRepository.findByName(chatName).orElseThrow {
            ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Chat not found"
            )
        }
    }

    override fun saveMessage(message: Message): Message {
        return messageRepository.save(message)
    }
}