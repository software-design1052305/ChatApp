package org.hse.web.socket.service

import org.hse.web.socket.model.Chat
import org.hse.web.socket.model.Message
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PathVariable
import java.util.*

@Service
interface MessageService {
    fun createChat(chat: Chat): Chat
    fun getChat(chatName: String): Chat
    fun saveMessage(message: Message):Message
}