package org.hse.web.socket.repository

import org.hse.web.socket.model.Message
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface MessageRepository: CrudRepository<Message, UUID>
