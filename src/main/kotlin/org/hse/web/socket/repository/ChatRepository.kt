package org.hse.web.socket.repository

import org.hse.web.socket.model.Chat
import org.springframework.data.repository.CrudRepository
import java.util.*

interface ChatRepository: CrudRepository<Chat, UUID>{
    fun findByName(chatName: String): Optional<Chat>
}
