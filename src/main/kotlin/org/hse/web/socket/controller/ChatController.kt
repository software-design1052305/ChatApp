package org.hse.web.socket.controller

import jakarta.validation.Valid
import org.hse.web.socket.model.Chat
import org.hse.web.socket.repository.ChatRepository
import org.hse.web.socket.repository.MessageRepository
import org.hse.web.socket.service.MessageService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
class ChatController(
    private val messageService: MessageService
) {

    @PostMapping("/api/chat/create")
    fun createChat(@RequestBody chat: Chat): Chat {
        return messageService.createChat(chat)
    }

    @GetMapping("/api/chat/get/{chatName}")
    fun getChat(@PathVariable chatName: String) =
        messageService.getChat(chatName)
}
