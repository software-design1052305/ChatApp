package org.hse.web.socket.controller

import org.hse.web.socket.model.Message
import org.hse.web.socket.service.MessageService
import org.springframework.messaging.handler.annotation.DestinationVariable
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import java.time.LocalDateTime
import java.util.*

@Controller
class WebSocketController(
    private val messageService: MessageService
) {

    @MessageMapping("/chat/{id}/send-message")
    @SendTo("/chat/{id}/receive-message")
    fun sendMessage(
        @DestinationVariable id: UUID,
        message: Message
    ) = messageService.saveMessage(message.apply {
        chatId = id
        messageTime = LocalDateTime.now()
    })

    @GetMapping("","/")
    fun default() = "redirect: createChat"

    @GetMapping("clear")
    fun clear() = "clear"

    @GetMapping("broker")
    fun broker() = "broker"

    @GetMapping("connectChat")
    fun connectChat() = "connectChat"

    @GetMapping("createChat")
    fun createChat() = "createChat"

    @GetMapping("getChat")
    fun getChat() = "getChat"
}
